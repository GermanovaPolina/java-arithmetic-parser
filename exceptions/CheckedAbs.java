package expression.exceptions;

import java.math.BigDecimal;
import expression.*;

public class CheckedAbs extends UnaryOperation {
    public CheckedAbs(GreatExpression value) {
        super(value);
    }


    @Override
    public String getSymbol() {
        return "abs";
    }

    @Override
    public int getPriority() {
        return 1;
    }

    @Override
    protected int count(int x)  {
        if (x == Integer.MIN_VALUE) {
            throw new OverflowError();
        }
        return x < 0 ? -x: x;
    }


}
