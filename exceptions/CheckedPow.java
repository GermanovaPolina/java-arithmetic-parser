package expression.exceptions;

import java.math.BigDecimal;
import expression.*;

public class CheckedPow extends BinaryOperation {
    public CheckedPow(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

    @Override
    protected int count(int x, int y) {
        if (x == 0 && y == 0 || y < 0) {
            throw new WrongOperationArgumentError();
        }
        int res = 1;
        while (y > 0)
        {
            if (y % 2 == 1) {
                res = new CheckedMultiply(new Const(res), new Const(x)).evaluate(0, 0, 0);
                y -= 1;
            } else {
                y /= 2;
                // System.err.println(x);
                x = new CheckedMultiply(new Const(x), new Const(x)).evaluate(0, 0, 0);
            }
        }
        return res;
    }

    @Override
    public String getSymbol() {
        return "**";
    }

    @Override
    public int getPriority() {
        return 2;
    }

}