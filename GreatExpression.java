package expression;

import java.math.BigDecimal;

import expression.generic.*;

public interface GreatExpression extends Expression, TripleExpression {
    int getPriority();
    String getSymbol();
    // default BigDecimal evaluate(BigDecimal d) { return BigDecimal.ZERO; }
    default boolean requiresChildBrackets() { return false; }
    default boolean requiresBrackets() { return false; }
    default <T extends Number> T genericEvaluate(ExpressionEvaluator<T> evaluator, T x, T y, T z) { return x;}
}