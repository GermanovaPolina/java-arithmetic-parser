package expression;

import expression.exceptions.*;
import expression.generic.*;

public class Count extends UnaryOperation {
    public Count(GreatExpression value) {
        super(value);
    }

    @Override
    public String getSymbol() {
        return "count";
    }

    @Override
    public int getPriority() {
        return 1;
    }

    @Override
    protected int count(int x)  {
        return Integer.bitCount(x);
    }

    @Override
    public <T extends Number> T genericEvaluate(ExpressionEvaluator<T> evaluator, T x, T y, T z) {
        return evaluator.bitCount(this.x.genericEvaluate(evaluator, x, y, z));
    }
}
