package expression.exceptions;

public class WrongOperationArgumentError extends CalculationError {
    public WrongOperationArgumentError() {
        super("Wrong argument(s) for the operation");
    }
}
