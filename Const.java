package expression;

import java.math.BigDecimal;
import java.math.RoundingMode;

import expression.generic.*;

public class Const implements GreatExpression {
    private final Number value;

    public Const(int value) {
        this.value = (Number) value;
    }


    @Override
    public int evaluate(int x) {
        return value.intValue();
    }

    @Override
    public int evaluate(int x, int y, int z) {
        return value.intValue();
    }

//    @Override
//    public BigDecimal evaluate(BigDecimal x) {
//        return (BigDecimal) value;
//    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Const) {
            Const c = (Const) obj;
            return this.value.equals(c.value);

        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public int getPriority() {
        return 0;
    }

    @Override
    public String getSymbol() {
        return this.toString();
    }


    @Override
    public <T extends Number> T genericEvaluate(ExpressionEvaluator<T> evaluator, T x, T y, T z) {
        return evaluator.getConst(value);
    }

    public static int priority() {
        return 0;
    }
}