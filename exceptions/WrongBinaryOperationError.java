package expression.exceptions;

public class WrongBinaryOperationError extends ExpressionError {
    public WrongBinaryOperationError() {
        super("Expected either operation or end of expression");
    }
}