package expression.parser;



public abstract class BaseParser {
    protected static final char END = 0;
    private CharSource source;
    private char ch;


    public void setSource(CharSource source) {
        this.source = source;
        take();
    }

    protected boolean test(final char expected) {
        return ch == expected;
    }

    protected boolean test(String s) {
        char startCh = ch;
        int startPos = source.getPosition();
        char curCh;
        for (int i = 0; i < s.length(); i++) {
            curCh = take();
            if (s.charAt(i) != curCh) {
                source.setPosition(startPos);
                ch = startCh;
                return false;
            }
        }
        return true;
    }

    protected char take() {
        final char result = ch;
        ch = source.hasNext() ? source.next() : END;
        return result;
    }

    protected boolean take(final char expected) {
        if (test(expected)) {
            take();
            return true;
        }
        return false;
    }

    protected boolean between(final char min, final char max) {
        return min <= ch && ch <= max;
    }

    protected boolean chWhitespace() {
        return Character.isWhitespace(ch);
    }

    protected void expect(char expected) {
        if (!take(expected)) {
            throw error("Expected '" + expected + "', found '" + ch + "'");
        }
    }

    protected void expect(final String value) {
        for (final char c : value.toCharArray()) {
            expect(c);
        }
    }

    protected IllegalArgumentException error(final String message) {
        return source.error(message);
    }

    protected void cout() {
        System.err.println(ch);
    }

}