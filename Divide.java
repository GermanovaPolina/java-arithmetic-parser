package expression;

import java.math.BigDecimal;

public class Divide extends BinaryOperation {
    public Divide(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

    @Override
    protected int count(int x, int y) {
        return x / y;
    }

//    @Override
//    protected BigDecimal bigDecimalCount(BigDecimal x, BigDecimal y) {
//        return x.divide(y);
//    }

    @Override
    public String getSymbol() {
        return "/";
    }

    @Override
    public int getPriority() {
        return 3;
    }

    @Override
    public boolean requiresChildBrackets() {
        return true;
    }

    @Override
    public boolean requiresBrackets() {
        return true;
    }

}