package expression;


import java.math.BigDecimal;

public class Add extends BinaryOperation {
    public Add(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

    @Override
    public String getSymbol() {
        return "+";
    }

    @Override
    protected int count(int x, int y) {
        return x + y;
    }

    @Override
    public int getPriority() {
        return 4;
    }

}