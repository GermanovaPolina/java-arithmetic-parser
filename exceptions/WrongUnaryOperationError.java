package expression.exceptions;

public class WrongUnaryOperationError extends ExpressionError {
    public WrongUnaryOperationError() {
        super("Wrong unary operation");
    }
}