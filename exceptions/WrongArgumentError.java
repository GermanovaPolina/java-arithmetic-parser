package expression.exceptions;

public class WrongArgumentError extends ExpressionError {
    public WrongArgumentError() {
        super("Wrong argument order");
    }
}