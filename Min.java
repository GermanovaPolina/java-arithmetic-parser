package expression;

import java.math.BigDecimal;
import expression.exceptions.*;
import expression.generic.*;

public class Min extends BinaryOperation {
    public Min(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

    @Override
    public String getSymbol() {
        return "min";
    }

    @Override
    public int getPriority() {
        return 5;
    }

    @Override
    protected int count(int x, int y) {
        return x < y ? x : y;
    }

    @Override
    public <T extends Number> T genericEvaluate(ExpressionEvaluator<T> evaluator, T x, T y, T z) {
        return evaluator.min(this.x.genericEvaluate(evaluator, x, y, z), this.y.genericEvaluate(evaluator, x, y, z));
    }

}