package expression.exceptions;

import java.util.*;
import java.lang.Exception;

import expression.parser.*;
import expression.*;

public class ExpressionParser extends BaseParser implements TripleParser {


    private final static Map<String, Integer> binaryOperations =  Map.ofEntries(Map.entry("**", 2),
            Map.entry("//", 2),  Map.entry("*", 3), Map.entry("/", 3),
            Map.entry("+", 4), Map.entry("-", 4), Map.entry(">>>", 5),
            Map.entry(">>", 5), Map.entry("<<", 5), Map.entry("min", 5), Map.entry("max", 5));
    private final Set<String> unaryOperations = Set.of("-", "t0", "l0", "abs", "count");

    SortedSet<String> strBinaryOperations = new TreeSet<String>(binaryOperations.keySet()).descendingSet();

    private String curOp = "";
    private int inExpression = 0;


    private void skipWhitespace() {
        while (chWhitespace()) {
            take();
        }
    }

    public GreatExpression parse(String s) throws ExpressionError {
        inExpression = 0;
        this.setSource(new StringSource(s));
        return parseExpression();
    }


    private GreatExpression parseExpression() throws ExpressionError {
        skipWhitespace();
        GreatExpression x = parseTerm(5);
        skipWhitespace();
        return x;
    }

    private GreatExpression parseTerm(int priority) throws ExpressionError {
        if (priority == 1) {
            return parseToken();
        }
        GreatExpression x;
        x = parseTerm(priority - 1);
        skipWhitespace();
        if (curOp.isEmpty()) {
            curOp = stringBinaryOperation();
        }

        skipWhitespace();
        if (curOp.isEmpty() && (!take(END) && !test(')') || test(')') && inExpression == 0)) {
            throw new WrongBinaryOperationError();
        }

        while (binaryOperations.getOrDefault(curOp, -1) == priority) {
            String temp = curOp;
            curOp = "";
            GreatExpression y = parseTerm(priority - 1);
            x = wrapBinaryOperation(temp, x, y);
            skipWhitespace();
            if (curOp.isEmpty()) {
                curOp = stringBinaryOperation();
            }

            skipWhitespace();
            if (curOp.isEmpty() && (!take(END) && !test(')') || test(')') && inExpression == 0)) {
                throw new WrongBinaryOperationError();
            }
        }
        return x;


    }

    private GreatExpression parseToken() throws ExpressionError {
        if (take('(')) {
            inExpression++;
            GreatExpression x = parseExpression();
            expect(')');
            inExpression--;

            return x;
        }
        if (take('-')) {
            if (between('0', '9')) {
                return parseConst(new StringBuilder("-"));
            }
            skipWhitespace();
            return new CheckedNegate(parseToken());
        }
        if (test('x') || test('y') || test('z')) {
            return new Variable(String.valueOf(take()));
        }
        if (between('0', '9')) {
            return parseConst();
        }
        String op = stringUnaryOperation();
        if (unaryOperations.contains(op)) {
            skipWhitespace();
            return wrapUnaryOperation(op, parseToken());
        }
        throw new WrongArgumentError();
    }



    private Const parseConst() throws ExpressionError {
        return parseConst(new StringBuilder());
    }

    private Const parseConst(StringBuilder number) throws ExpressionError {
        while (between('0', '9')) {
            number.append(take());
        }
        return new Const(Integer.parseInt(number.toString()));
    }

    private GreatExpression wrapUnaryOperation(String s, GreatExpression x) throws ExpressionError {
        switch (s) {
            case "t0" :  return new LowZero(x);
            case "l0" :  return new HighZero(x);
            case "abs" : return new CheckedAbs(x);
            case "count": return new Count(x);
            default : throw new ExpressionError("Unknown error");
        }
    }

    private GreatExpression wrapBinaryOperation(String s, GreatExpression x, GreatExpression y) throws ExpressionError {
        switch (s) {
            case "+" : return new CheckedAdd(x, y);
            case "-" : return new CheckedSubtract(x, y);
            case "*" : return new CheckedMultiply(x, y);
            case "/" : return new CheckedDivide(x, y);
            case "<<" : return new LShift(x, y);
            case ">>" : return new RShift(x, y);
            case ">>>" : return new ARShift(x, y);
            case "**" : return new CheckedPow(x, y);
            case "//" : return new CheckedLog(x, y);
            case "min": return new Min(x, y);
            case "max": return new Max(x, y);
            default : throw new ExpressionError("Unknown error");
        }
    }


    private String stringUnaryOperation() throws ExpressionError {
        if (test("t0")) {
            return "t0";
        }
        else if (test("l0")) {
            return "l0";
        } else if (test("abs")) {
            if (!test('(') && !chWhitespace()) {
                throw new WrongUnaryOperationError();
            }
            return "abs";
        } else if (test("count")) {
            if (!test('(') && !chWhitespace()) {
                throw new WrongUnaryOperationError();
            }
            return "count";
        }
        return "";
    }

    private String stringBinaryOperation() throws ExpressionError {
        StringBuilder sb = new StringBuilder();
        skipWhitespace();
        for (String op : strBinaryOperations) {
            if (test(op)) {
                return op;
            }
        }
        if (!sb.isEmpty()) {
            throw new WrongBinaryOperationError();
        }
        return "";
    }
}