package expression.generic;

import expression.exceptions.*;

import java.math.BigInteger;

public class LongEvaluator implements ExpressionEvaluator<Long> {
    @Override
    public Long add(Long x, Long y) {
        return x + y;
    }

    @Override
    public Long subtract(Long x, Long y) {
        return x - y;
    }

    @Override
    public Long multiply(Long x, Long y) {
        return x * y;
    }

    @Override
    public Long divide(Long x, Long y) {
        if (y == 0) {
            throw new DivisionByZeroError();
        }
        return x / y;
    }

    @Override
    public Long negate(Long x) {
        return -1 * x;
    }

    @Override
    public Long getConst(Number val) {
        return Long.parseLong(String.valueOf(val));
    }

    @Override
    public Long bitCount(Long x) {
        return (long) Long.bitCount(x);
    }

    @Override
    public Long min(Long x, Long y) {
        return Long.min(x, y);
    }

    @Override
    public Long max(Long x, Long y) {
        return Long.max(x, y);
    }
}
