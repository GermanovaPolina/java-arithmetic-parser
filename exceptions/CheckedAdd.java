package expression.exceptions;


import java.math.BigDecimal;
import java.lang.Exception;
import expression.*;
import expression.generic.*;

public class CheckedAdd extends BinaryOperation {
    public CheckedAdd(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

    @Override
    public String getSymbol() {
        return "+";
    }

//    @Override
//    protected BigDecimal bigDecimalCount(BigDecimal x, BigDecimal y) {
//        return x.add(y);
//    }

    @Override
    public int getPriority() {
        return 4;
    }



    protected int count(int x, int y) {
        if ((x > 0 && y > 0 && Integer.MAX_VALUE - y < x) || (x < 0 && y < 0 && (x == Integer.MIN_VALUE
                || y == Integer.MIN_VALUE || x < Integer.MIN_VALUE - y))) {
            throw new OverflowError();
        }
        return x + y;
    }

    @Override
    public <T extends Number> T genericEvaluate(ExpressionEvaluator<T> evaluator, T x, T y, T z) {
        return evaluator.add(this.x.genericEvaluate(evaluator, x, y, z), this.y.genericEvaluate(evaluator, x, y, z));
    }


}