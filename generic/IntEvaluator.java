package expression.generic;

import expression.exceptions.*;

import java.math.BigInteger;

public class IntEvaluator implements ExpressionEvaluator<Integer> {
    @Override
    public Integer add(Integer x, Integer y) {
        return x + y;
    }

    @Override
    public Integer subtract(Integer x, Integer y) {
        return x - y;
    }

    @Override
    public Integer multiply(Integer x, Integer y) {
        return x * y;
    }

    @Override
    public Integer divide(Integer x, Integer y) {
        if (y == 0) {
            throw new DivisionByZeroError();
        }
        return x / y;
    }

    @Override
    public Integer negate(Integer x) {
        return -1 * x;
    }

    @Override
    public Integer getConst(Number val) {
        return Integer.parseInt(String.valueOf(val));
    }

    @Override
    public Integer bitCount(Integer x) {
        return Integer.bitCount(x);
    }

    @Override
    public Integer min(Integer x, Integer y) {
        return Integer.min(x, y);
    }

    @Override
    public Integer max(Integer x, Integer y) {
        return Integer.max(x, y);
    }
}
