package expression;

import java.math.BigDecimal;

public class Log extends BinaryOperation {
    public Log(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

    @Override
    protected int count(int x, int y) {
        int res = 0;
        while (x >= y) {
            res++;
            x /= y;
        }
        return res;
    }

    @Override
    public String getSymbol() {
        return "//";
    }

    @Override
    public int getPriority() {
        return 2;
    }

}