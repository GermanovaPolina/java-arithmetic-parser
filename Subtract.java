package expression;

import java.math.BigDecimal;

public class Subtract extends BinaryOperation {
    public Subtract(GreatExpression x, GreatExpression y) {
        super(x, y);
    }


    @Override
    protected int count(int x, int y) {
        return x - y;
    }

//    @Override
//    protected BigDecimal bigDecimalCount(BigDecimal x, BigDecimal y) {
//        return x.subtract(y);
//    }

    @Override
    public String getSymbol() {
        return "-";
    }

    @Override
    public int getPriority() {
        return 4;
    }

    @Override
    public boolean requiresChildBrackets() {
        return true;
    }

}