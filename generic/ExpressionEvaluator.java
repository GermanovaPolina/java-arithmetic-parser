package expression.generic;

import expression.*;
import expression.parser.*;
import expression.exceptions.*;

public interface ExpressionEvaluator<T> {
    T add(T x, T y);
    T subtract(T x, T y);
    T multiply(T x, T y);
    T divide(T x, T y);
    T negate(T x);
    T getConst(Number val);
    T bitCount(T x);
    T min(T x, T y);
    T max(T x, T y);
}
