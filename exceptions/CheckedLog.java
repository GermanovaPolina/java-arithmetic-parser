package expression.exceptions;

import java.math.BigDecimal;
import expression.*;

public class CheckedLog extends BinaryOperation {
    public CheckedLog(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

    @Override
    protected int count(int x, int y) {
        if (x <= 0 || y <= 0 || y == 1) {
            throw new WrongOperationArgumentError();
        }
        int res = 0;
        while (x >= y) {
            res++;
            x /= y;
        }
        return res;
    }

    @Override
    public String getSymbol() {
        return "//";
    }

    @Override
    public int getPriority() {
        return 2;
    }

}