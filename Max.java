package expression;

import java.math.BigDecimal;
import expression.exceptions.*;
import expression.generic.*;

public class Max extends BinaryOperation {
    public Max(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

    @Override
    public String getSymbol() {
        return "max";
    }

    @Override
    public int getPriority() {
        return 5;
    }

    @Override
    protected int count(int x, int y) {
        return x > y ? x : y;
    }

    @Override
    public <T extends Number> T genericEvaluate(ExpressionEvaluator<T> evaluator, T x, T y, T z) {
        return evaluator.max(this.x.genericEvaluate(evaluator, x, y, z), this.y.genericEvaluate(evaluator, x, y, z));
    }

}