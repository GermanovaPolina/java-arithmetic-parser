package expression;

import java.math.BigDecimal;

public class Abs extends UnaryOperation {
    public Abs(GreatExpression value) {
        super(value);
    }

    @Override
    protected int count(int val) {
        return val < 0 ? -val: val;
    }

    @Override
    public String getSymbol() {
        return "abs";
    }


    @Override
    public int getPriority() {
        return 1;
    }
}
