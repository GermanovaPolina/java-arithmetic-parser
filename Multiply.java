package expression;

import java.math.BigDecimal;

public class Multiply extends BinaryOperation {
    public Multiply(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

    @Override
    protected int count(int x, int y) {
        return x * y;
    }

//    @Override
//    protected BigDecimal bigDecimalCount(BigDecimal x, BigDecimal y) {
//        return x.multiply(y);
//    }

    @Override
    public String getSymbol() {
        return "*";
    }

    @Override
    public int getPriority() {
        return 3;
    }

}