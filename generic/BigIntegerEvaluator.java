package expression.generic;


import java.math.BigInteger;
import expression.exceptions.*;

public class BigIntegerEvaluator implements ExpressionEvaluator<BigInteger> {
    @Override
    public BigInteger add(BigInteger x, BigInteger y) {
        return x.add(y);
    }

    @Override
    public BigInteger subtract(BigInteger x, BigInteger y) {
        return x.subtract(y);
    }

    @Override
    public BigInteger multiply(BigInteger x, BigInteger y) {
        return x.multiply(y);
    }

    @Override
    public BigInteger divide(BigInteger x, BigInteger y) {
        if (y.equals(BigInteger.ZERO)) {
            throw new DivisionByZeroError();
        }
        return x.divide(y);
    }

    @Override
    public BigInteger negate(BigInteger x) {
        return x.negate();
    }

    @Override
    public BigInteger getConst(Number val) {
        return new BigInteger(String.valueOf(val));
    }

    @Override
    public BigInteger bitCount(BigInteger x) {
        return BigInteger.valueOf(x.bitCount());
    }

    @Override
    public BigInteger min(BigInteger x, BigInteger y) {
        return x.compareTo(y) == -1 ? x : y;
    }

    @Override
    public BigInteger max(BigInteger x, BigInteger y) {
        return x.compareTo(y) == 1 ? x : y;
    }
}
