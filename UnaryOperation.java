package expression;

import java.math.BigDecimal;

public abstract class UnaryOperation implements GreatExpression {
    protected final GreatExpression x;

    public UnaryOperation(GreatExpression x) {
        this.x = x;
    }

    protected abstract int count(int x);
    @Override
    public String toString() {
        return this.getSymbol() + "(" + x.toString() + ")";
    }

    @Override
    public int evaluate(int x) {
        return this.count(this.x.evaluate(x));
    }

    @Override
    public int evaluate(int x, int y, int z) {
        return this.count(this.x.evaluate(x, y, z));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass()) {
            UnaryOperation ex = (UnaryOperation) obj;
            return this.x.equals(ex.x);
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return 13 * x.hashCode() + 11 * this.getSymbol().hashCode();
    }

    @Override
    public String toMiniString() {
        String s = this.getSymbol();
        if (x.getPriority() != 0 && x.getPriority() != 1) {
            s += "(" + x.toMiniString() + ")";
        } else {
            s += " " + x.toMiniString();
        }
        return s;
    }

}