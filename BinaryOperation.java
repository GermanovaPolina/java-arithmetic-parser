package expression;

import java.math.BigDecimal;


public abstract class BinaryOperation implements GreatExpression {
    protected final GreatExpression x;
    protected final GreatExpression y;

    public BinaryOperation(final GreatExpression x, final GreatExpression y) {
        this.x = x;
        this.y = y;
    }

    protected abstract int count(int x, int y);

    @Override
    public String toString() {
        return "(" + x.toString() + " " + this.getSymbol() + " " + y.toString() + ")";
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj != null && obj.getClass() == this.getClass()) {
            final BinaryOperation ex = (BinaryOperation) obj;
            return this.x.equals(ex.x) && this.y.equals(ex.y);
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return 13 * x.hashCode() - 43 * y.hashCode() + 11 * this.getClass().getName().hashCode();
    }

    @Override
    public int evaluate(final int x) {
        return this.count(this.x.evaluate(x), this.y.evaluate(x));
    }

    @Override
    public int evaluate(final int x, final int y, final int z) {
        return this.count(this.x.evaluate(x, y, z), this.y.evaluate(x, y, z));
    }

    @Override
    public String toMiniString() {
        String s;
        if (this.getPriority() >= x.getPriority()) {
            s = x.toMiniString();
        } else {
            s = "(" + x.toMiniString() + ")";
        }

        s += " " + this.getSymbol() + " ";

        if (getPriority() < y.getPriority() || (this.getPriority() == y.getPriority() &&
                                                (requiresChildBrackets() || y.requiresBrackets()))) {
            s += "(" + y.toMiniString() + ")";
        } else {
            s += y.toMiniString();
        }
        return s;
    }

}