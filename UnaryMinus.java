package expression;

import java.math.BigDecimal;

public class UnaryMinus extends UnaryOperation {

    public UnaryMinus(GreatExpression value) {
        super(value);
    }

    @Override
    protected int count(int x) {
        return -1 * x;
    }


    @Override
    public String getSymbol() {
        return "-";
    }

    @Override
    public int getPriority() {
        return 1;
    }


}