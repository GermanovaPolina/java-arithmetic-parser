package expression.generic;

import expression.exceptions.*;

import java.math.BigInteger;

public class TabulateEvaluator implements ExpressionEvaluator<Integer> {
    private static Integer trim(Integer x) {
        return x - x % 10;
    }

    @Override
    public Integer add(Integer x, Integer y) {
        return trim(trim(x) + trim(y));
    }

    @Override
    public Integer subtract(Integer x, Integer y) {
        return trim(trim(x) - trim(y));
    }

    @Override
    public Integer multiply(Integer x, Integer y) {
        return trim(trim(x) * trim(y));
    }

    @Override
    public Integer divide(Integer x, Integer y) {
        if (trim(y) == 0) {
            throw new DivisionByZeroError();
        }
        return trim(trim(x) / trim(y));
    }

    @Override
    public Integer negate(Integer x) {
        return -1 * trim(x);
    }

    @Override
    public Integer getConst(Number val) {
        return trim(Integer.parseInt(String.valueOf(val)));
    }

    @Override
    public Integer bitCount(Integer x) {
        return trim(Integer.bitCount(x));
    }

    @Override
    public Integer min(Integer x, Integer y) {
        return Integer.min(trim(x), trim(y));
    }

    @Override
    public Integer max(Integer x, Integer y) {
        return Integer.max(trim(x), trim(y));
    }
}
