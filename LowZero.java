package expression;


public class LowZero extends UnaryOperation {

    public LowZero(GreatExpression value) {
        super(value);
    }

    @Override
    protected int count(int val) {
        return  Integer.numberOfTrailingZeros(val);
    }

    @Override
    public String getSymbol() {
        return "t0";
    }


    @Override
    public int getPriority() {
        return 1;
    }


}