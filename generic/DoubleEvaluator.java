package expression.generic;


import java.math.BigInteger;
import expression.exceptions.*;

public class DoubleEvaluator implements ExpressionEvaluator<Double> {
    @Override
    public Double add(Double x, Double y) {
        return x + y;
    }

    @Override
    public Double subtract(Double x, Double y) {
        return x - y;
    }
    @Override
    public Double multiply(Double x, Double y) {
        return x * y;
    }
    @Override
    public Double divide(Double x, Double y) {
        return x / y;
    }
    @Override
    public Double negate(Double x) {
        return -1 * x;
    }

    @Override
    public Double getConst(Number val) {
        return Double.parseDouble(String.valueOf(val));
    }

    @Override
    public Double bitCount(Double x) {
        return (double) Long.bitCount(Double.doubleToLongBits(x));
    }

    @Override
    public Double min(Double x, Double y) {
        return Double.min(x, y);
    }

    @Override
    public Double max(Double x, Double y) {
        return Double.max(x, y);
    }
}
