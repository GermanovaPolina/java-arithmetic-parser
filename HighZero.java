package expression;

public class HighZero extends UnaryOperation {

    public HighZero(GreatExpression value) {
        super(value);
    }

    @Override
    protected int count(int val) {
        return Integer.numberOfLeadingZeros(val);
    }

    @Override
    public String getSymbol() {
        return "l0";
    }


    @Override
    public int getPriority() {
        return 1;
    }

}