package expression;

import java.math.BigDecimal;
import expression.generic.*;

public class Variable implements GreatExpression {
    private final String value;

    public Variable(String value) {
        this.value = value;
    }

    @Override
    public int evaluate(int x) {
        return x;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Variable) {
            Variable var = (Variable) obj;
            return this.value.equals(var.value);
        }
        return super.equals(obj);
    }

    @Override
    public int evaluate(int x, int y, int z) {
        if ("x".equals(value)) {
            return x;
        } else if (value.equals("y")) {
            return y;
        }
        return z;
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public int getPriority() {
        return 0;
    }

    @Override
    public String getSymbol() {
        return this.value;
    }
//
//    @Override
//    public BigDecimal evaluate(BigDecimal x) {
//        return x;
//    }

    @Override
    public <T extends Number> T genericEvaluate(ExpressionEvaluator<T> evaluator, T x, T y, T z) {
        if (value.equals("x")) {
            return evaluator.getConst(x);
        } else if (value.equals("y")) {
            return evaluator.getConst(y);
        }
        return evaluator.getConst(z);
    }
}
