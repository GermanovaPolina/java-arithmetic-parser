package expression.exceptions;

import java.math.BigDecimal;
import expression.*;
import expression.generic.*;

public class CheckedDivide extends BinaryOperation {
    public CheckedDivide(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

//    @Override
//    protected BigDecimal bigDecimalCount(BigDecimal x, BigDecimal y) {
//        return x.divide(y);
//    }

    @Override
    public String getSymbol() {
        return "/";
    }

    @Override
    public int getPriority() {
        return 3;
    }

    @Override
    public boolean requiresChildBrackets() {
        return true;
    }

    @Override
    public boolean requiresBrackets() {
        return true;
    }

    @Override
    protected int count(int x, int y) {
        if (y == 0) {
            throw new DivisionByZeroError();
        }
        if (x == Integer.MIN_VALUE && y == -1) {
            throw new OverflowError();
        }
        return x / y;
    }

    @Override
    public <T extends Number> T genericEvaluate(ExpressionEvaluator<T> evaluator, T x, T y, T z) {
        return evaluator.divide(this.x.genericEvaluate(evaluator, x, y, z), this.y.genericEvaluate(evaluator, x, y, z));
    }

}