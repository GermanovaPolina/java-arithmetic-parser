package expression.generic;

import expression.exceptions.*;

import java.math.BigInteger;

public class CheckedIntEvaluator extends IntEvaluator {
    @Override
    public Integer add(Integer x, Integer y) {
        if ((x > 0 && y > 0 && java.lang.Integer.MAX_VALUE - y < x)||(x < 0 && y < 0 && (x == java.lang.Integer.MIN_VALUE
                || y == java.lang.Integer.MIN_VALUE || x < java.lang.Integer.MIN_VALUE - y))) {
            throw new OverflowError();
        }
        return super.add(x, y);
    }

    @Override
    public Integer subtract(Integer x, Integer y) {
        if ((y == Integer.MIN_VALUE && x >= 0) || (x > 0 && y < 0 && Integer.MAX_VALUE + y < x) ||
                (x < 0 && y > 0 && (x == Integer.MIN_VALUE || x < Integer.MIN_VALUE + y))) {
            throw new OverflowError();
        }
        return super.subtract(x, y);
    }

    @Override
    public Integer multiply(Integer x, Integer y) {
        if ((x > 0 && y > 0 && x > Integer.MAX_VALUE / y) ||
                (x < 0 && y < 0 && (y == Integer.MIN_VALUE || x < Integer.MAX_VALUE / y)) ||
                (x <  0 && y > 0 && x < Integer.MIN_VALUE / y) || (x > 0 && y < 0 && y < Integer.MIN_VALUE / x) ) {
            throw new OverflowError();
        }
        return super.multiply(x, y);
    }

    @Override
    public Integer divide(Integer x, Integer y) {
        if (x == Integer.MIN_VALUE && y == -1) {
            throw new OverflowError();
        }
        return super.divide(x, y);
    }

    @Override
    public Integer negate(Integer x) {
        if (x == Integer.MIN_VALUE) {
            throw new OverflowError();
        }
        return super.negate(x);
    }
}
