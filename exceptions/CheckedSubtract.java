package expression.exceptions;

import java.math.BigDecimal;
import expression.*;
import expression.generic.*;

public class CheckedSubtract extends BinaryOperation {
    public CheckedSubtract(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

//    @Override
//    protected BigDecimal bigDecimalCount(BigDecimal x, BigDecimal y) {
//        return x.subtract(y);
//    }

    @Override
    public String getSymbol() {
        return "-";
    }

    @Override
    public int getPriority() {
        return 4;
    }

    @Override
    public boolean requiresChildBrackets() {
        return true;
    }

    @Override
    protected int count(int x, int y)   {
        if ((y == Integer.MIN_VALUE && x >= 0) || (x > 0 && y < 0 && Integer.MAX_VALUE + y < x) ||
                (x < 0 && y > 0 && (x == Integer.MIN_VALUE || x < Integer.MIN_VALUE + y))) {
            throw new OverflowError();
        }
        return x - y;
    }

    @Override
    public <T extends Number> T genericEvaluate(ExpressionEvaluator<T> evaluator, T x, T y, T z) {
        return evaluator.subtract(this.x.genericEvaluate(evaluator, x, y, z),
                this.y.genericEvaluate(evaluator, x, y, z));
    }

}