package expression.generic;

import expression.*;
import expression.parser.*;
import expression.exceptions.*;

public class GenericTabulator implements Tabulator {
    private final ExpressionParser parser;

    public GenericTabulator() {
        this.parser = new ExpressionParser();
    }

    @Override
    public Object[][][] tabulate(String mode, String expression, int x1, int x2, int y1, int y2, int z1, int z2)
            throws Exception {
        GreatExpression resultExpression = parser.parse(expression);
        ExpressionEvaluator evaluator = switch (mode) {
            case "i" -> new CheckedIntEvaluator();
            case "d" -> new DoubleEvaluator();
            case "bi" -> new BigIntegerEvaluator();
            case "u" -> new IntEvaluator();
            case "l" -> new LongEvaluator();
            default -> new TabulateEvaluator();
        };
        Object[][][] result = new Object[x2 - x1 + 1][y2 - y1 + 1][z2 - z1 + 1];
        for (int x = 0; x <= x2 - x1; x++) {
            for (int y = 0; y <= y2 - y1; y++) {
                for (int z = 0; z <= z2 - z1; z++) {
                    try {
                        result[x][y][z] = resultExpression.genericEvaluate(
                                evaluator, 
                                x1 + x, y1 + y, z1 + z);
                    } catch (CalculationError e) {
                        result[x][y][z] = null;
                    }
                }
            }
        }
        return result;
    }

}
