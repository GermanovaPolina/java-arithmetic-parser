package expression.exceptions;

public class DivisionByZeroError extends CalculationError {
    public DivisionByZeroError() {
        super("Division by zero");
    }
}
