package expression.exceptions;

public class ExpressionError extends Exception {
    public ExpressionError(String message) {
        super(message);
    }
}
