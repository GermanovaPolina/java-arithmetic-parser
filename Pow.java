package expression;

import java.math.BigDecimal;

public class Pow extends BinaryOperation {
    public Pow(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

    @Override
    protected int count(int x, int y) {
        int res = 1;
        while (y > 0)
        {
            if (y % 2 == 1) {
                res *= x;
                y -= 1;
            } else {
                y /= 2;
                x *= x;
            }
        }
        return res;
    }

    @Override
    public String getSymbol() {
        return "**";
    }

    @Override
    public int getPriority() {
        return 2;
    }

}