package expression.exceptions;

public class OverflowError extends CalculationError {
    public OverflowError() {
        super("Overflow");
    }
}
