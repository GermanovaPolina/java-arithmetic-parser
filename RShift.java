package expression;

import java.math.BigDecimal;

public class RShift extends BinaryOperation {
    public RShift(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

    @Override
    protected int count(int x, int y) {
        return x >> y;
    }

    @Override
    public String getSymbol() {
        return ">>";
    }

    @Override
    public int getPriority() {
        return 5;
    }

    @Override
    public boolean requiresChildBrackets() {
        return true;
    }

    @Override
    public boolean requiresBrackets() {
        return true;
    }

}
