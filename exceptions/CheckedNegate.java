package expression.exceptions;

import java.math.BigDecimal;
import expression.*;
import expression.generic.*;

public class CheckedNegate extends UnaryOperation {

    public CheckedNegate(GreatExpression value) {
        super(value);
    }


    @Override
    public String getSymbol() {
        return "-";
    }

    @Override
    public int getPriority() {
        return 1;
    }

    @Override
    protected int count(int x)  {
        if (x == Integer.MIN_VALUE) {
            throw new OverflowError();
        }
        return -1 * x;
    }

    @Override
    public <T extends Number> T genericEvaluate(ExpressionEvaluator<T> evaluator, T x, T y, T z) {
        return evaluator.negate(this.x.genericEvaluate(evaluator, x, y, z));
    }
}