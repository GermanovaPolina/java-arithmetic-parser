package expression.exceptions;

public class CalculationError extends RuntimeException {
    public CalculationError(String message) {
        super(message);
    }
}
