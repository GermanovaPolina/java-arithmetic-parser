package expression.exceptions;

import java.math.BigDecimal;
import expression.*;
import expression.generic.*;

public class CheckedMultiply extends BinaryOperation {
    public CheckedMultiply(GreatExpression x, GreatExpression y) {
        super(x, y);
    }

//    @Override
//    protected BigDecimal bigDecimalCount(BigDecimal x, BigDecimal y) {
//        return x.multiply(y);
//    }

    @Override
    public String getSymbol() {
        return "*";
    }

    @Override
    public int getPriority() {
        return 3;
    }

    @Override
    protected int count(int x, int y) {
        if ((x > 0 && y > 0 && x > Integer.MAX_VALUE / y) ||
                (x < 0 && y < 0 && (y == Integer.MIN_VALUE || x < Integer.MAX_VALUE / y)) ||
                (x <  0 && y > 0 && x < Integer.MIN_VALUE / y) || (x > 0 && y < 0 && y < Integer.MIN_VALUE / x) ) {
            throw new OverflowError();
        }
        return x * y;
    }

    @Override
    public <T extends Number> T genericEvaluate(ExpressionEvaluator<T> evaluator, T x, T y, T z) {
        return evaluator.multiply(this.x.genericEvaluate(evaluator, x, y, z),
                this.y.genericEvaluate(evaluator, x, y, z));
    }

}